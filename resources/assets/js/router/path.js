import Wrapper from '@/components/Wrapper';

export default [
    {
        path: '/',
        component: Wrapper,
        children: [
          {
            path: '',
            component: () => import(
              `@/pages/form/MainPage.vue`
            ),
          },
        ],
    },
    {
        path: '/question_settings',
        component: Wrapper,
        children: [
          {
            path: '',
            component: () => import(
              `@/pages/question_settings/MainPage.vue`
            ),
          },
        ],
    },
    {
      path: '/answer_list',
      component: Wrapper,
      children: [
        {
          path: '',
          component: () => import(
            `@/pages/answer_list/MainPage.vue`
          ),
        },
      ],
  },
]

